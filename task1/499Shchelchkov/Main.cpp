#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <Mesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>
#include <vector>
#include <stdlib.h>
/**
Куб и кролик. Управление виртуальной камерой. Вращение кролика.
*/

class Node {
	std::vector<bool> walls; // front, left, top, back, right, bottom
	glm::vec3 center;
	MeshPtr mesh;
	float size;
	public:
		Node(const glm::vec3 center): walls(6, true), mesh(std::make_shared<Mesh>()), center(center), size(0.499) {
		}
		glm::vec3 getCenter() {
			return center;
		}
		float getSize() {
			return size;
		}
		std::vector<bool> getWalls() {
			return walls;
		}
		void removeWall(int wallIdx) {
			if ((wallIdx < 0) || !(wallIdx < walls.size())) {
				std::cerr << "Wrong wall idx:" <<  wallIdx << ". wallIdx shoulf be in range [0, " <<
					walls.size() << ")." << std::endl;
				throw "index is out of range";
			}
			walls[wallIdx] = false;
		}
		
		void init() {
			std::vector<glm::vec3> vertices;
			std::vector<glm::vec3> normals;
			std::vector<glm::vec2> texcoords;

			//front 1
			if (walls[0]) {
				vertices.push_back(glm::vec3(size, -size, size));
				vertices.push_back(glm::vec3(size, size, -size));
				vertices.push_back(glm::vec3(size, size, size));

				normals.push_back(glm::vec3(1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(1.0, 0.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
				texcoords.push_back(glm::vec2(1.0, 1.0));

				//front 2
				vertices.push_back(glm::vec3(size, -size, size));
				vertices.push_back(glm::vec3(size, -size, -size));
				vertices.push_back(glm::vec3(size, size, -size));

				normals.push_back(glm::vec3(1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(1.0, 0.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(0.0, 0.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
			}
			if (walls[1]) {
				//left 1
				vertices.push_back(glm::vec3(-size, -size, size));
				vertices.push_back(glm::vec3(size, -size, -size));
				vertices.push_back(glm::vec3(size, -size, size));

				normals.push_back(glm::vec3(0.0, -1.0, 0.0));
				normals.push_back(glm::vec3(0.0, -1.0, 0.0));
				normals.push_back(glm::vec3(0.0, -1.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
				texcoords.push_back(glm::vec2(1.0, 1.0));

				//left 2
				vertices.push_back(glm::vec3(-size, -size, size));
				vertices.push_back(glm::vec3(-size, -size, -size));
				vertices.push_back(glm::vec3(size, -size, -size));

				normals.push_back(glm::vec3(0.0, -1.0, 0.0));
				normals.push_back(glm::vec3(0.0, -1.0, 0.0));
				normals.push_back(glm::vec3(0.0, -1.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(0.0, 0.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
			}
			if (walls[2]) {
				//top 1
				vertices.push_back(glm::vec3(-size, size, size));
				vertices.push_back(glm::vec3(size, -size, size));
				vertices.push_back(glm::vec3(size, size, size));

				normals.push_back(glm::vec3(0.0, 0.0, 1.0));
				normals.push_back(glm::vec3(0.0, 0.0, 1.0));
				normals.push_back(glm::vec3(0.0, 0.0, 1.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
				texcoords.push_back(glm::vec2(1.0, 1.0));

				//top 2
				vertices.push_back(glm::vec3(-size, size, size));
				vertices.push_back(glm::vec3(-size, -size, size));
				vertices.push_back(glm::vec3(size, -size, size));

				normals.push_back(glm::vec3(0.0, 0.0, 1.0));
				normals.push_back(glm::vec3(0.0, 0.0, 1.0));
				normals.push_back(glm::vec3(0.0, 0.0, 1.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(0.0, 0.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
			}
			
			if (walls[3]) {
				//back 1
				vertices.push_back(glm::vec3(-size, -size, size));
				vertices.push_back(glm::vec3(-size, size, size));
				vertices.push_back(glm::vec3(-size, size, -size));

				normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));

				//back 2
				vertices.push_back(glm::vec3(-size, -size, size));
				vertices.push_back(glm::vec3(-size, size, -size));
				vertices.push_back(glm::vec3(-size, -size, -size));

				normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
				normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
				texcoords.push_back(glm::vec2(0.0, 0.0));
			}
			if (walls[4]) {
				//right 1
				vertices.push_back(glm::vec3(-size, size, size));
				vertices.push_back(glm::vec3(size, size, size));
				vertices.push_back(glm::vec3(size, size, -size));

				normals.push_back(glm::vec3(0.0, 1.0, 0.0));
				normals.push_back(glm::vec3(0.0, 1.0, 0.0));
				normals.push_back(glm::vec3(0.0, 1.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));

				//right 2
				vertices.push_back(glm::vec3(-size, size, size));
				vertices.push_back(glm::vec3(+size, size, -size));
				vertices.push_back(glm::vec3(-size, size, -size));

				normals.push_back(glm::vec3(0.0, 1.0, 0.0));
				normals.push_back(glm::vec3(0.0, 1.0, 0.0));
				normals.push_back(glm::vec3(0.0, 1.0, 0.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
				texcoords.push_back(glm::vec2(0.0, 0.0));
			}
			if (walls[5]) {
				//bottom 1
				vertices.push_back(glm::vec3(-size, size, -size));
				vertices.push_back(glm::vec3(size, size, -size));
				vertices.push_back(glm::vec3(size, -size, -size));

				normals.push_back(glm::vec3(0.0, 0.0, -1.0));
				normals.push_back(glm::vec3(0.0, 0.0, -1.0));
				normals.push_back(glm::vec3(0.0, 0.0, -1.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));

				//bottom 2
				vertices.push_back(glm::vec3(-size, size, -size));
				vertices.push_back(glm::vec3(size, -size, -size));
				vertices.push_back(glm::vec3(-size, -size, -size));

				normals.push_back(glm::vec3(0.0, 0.0, -1.0));
				normals.push_back(glm::vec3(0.0, 0.0, -1.0));
				normals.push_back(glm::vec3(0.0, 0.0, -1.0));

				texcoords.push_back(glm::vec2(0.0, 1.0));
				texcoords.push_back(glm::vec2(1.0, 0.0));
				texcoords.push_back(glm::vec2(0.0, 0.0));
			}
			//----------------------------------------
			for (auto& vertex: vertices) {
				vertex += center;
			}
			DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
			buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

			DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
			buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

			DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
			buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

			mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
			mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
			mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
			mesh->setPrimitiveType(GL_TRIANGLES);
			mesh->setVertexCount(vertices.size());

			std::cout << "Node is created with " << vertices.size() << " walls\n";
		}
	const MeshPtr getMesh() {
		return mesh;
	}
	//~ const getWallVertices(int wallIdx) {
		//~ if ((wallIdx < 0) || !(wallIdx < walls.size())) {
			//~ std::cerr << "Wrong wall idx:" << wallIdx << ". wallIdx shoulf be in range [0, " <<
					  //~ walls.size() << ")." << std::endl;
			//~ throw "index is out of range";
		//~ }
		//~ std::vector<glm::vec3> result;
		//~ switch (wallIdx) {
			//~ case 0:
				//~ result.emplace_back(glm::vec3(size, -size, size));
				//~ result.emplace_back(glm::vec3(size, size, -size));
				//~ result.emplace_back(glm::vec3(size, size, size));
				//~ result.emplace_back(glm::vec3(size, -size, -size));
				//~ break;
			//~ case 1:
				//~ //left 1
				//~ result.emplace_back(glm::vec3(-size, -size, size));
				//~ result.emplace_back(glm::vec3(size, -size, -size));
				//~ result.emplace_back(glm::vec3(size, -size, size));
				//~ result.emplace_back(glm::vec3(-size, -size, -size));
				//~ break;
			//~ case 2:
				//~ //top 1
				//~ result.emplace_back(glm::vec3(-size, size, size));
				//~ result.emplace_back(glm::vec3(size, -size, size));
				//~ result.emplace_back(glm::vec3(size, size, size));
				//~ result.emplace_back(glm::vec3(-size, -size, size));
				//~ break;
			//~ case 3:
				//~ result.emplace_back(glm::vec3(-size, -size, size));
				//~ result.emplace_back(glm::vec3(-size, size, size));
				//~ result.emplace_back(glm::vec3(-size, size, -size));
				//~ result.emplace_back(glm::vec3(-size, size, -size));
				//~ break;
			//~ case 4:
				//~ result.emplace_back(glm::vec3(-size, size, size));
				//~ result.emplace_back(glm::vec3(size, size, size));
				//~ result.emplace_back(glm::vec3(size, size, -size));
				//~ result.emplace_back(glm::vec3(+size, size, -size));
				//~ break;
			//~ case 5:
				//~ result.emplace_back(glm::vec3(-size, size, -size));
				//~ result.emplace_back(glm::vec3(size, size, -size));
				//~ result.emplace_back(glm::vec3(size, -size, -size));
				//~ result.emplace_back(glm::vec3(size, -size, -size));
				//~ break;
			//~ default:
				//~ throw "how did you get there?";
		//~ }
		//~ for (auto &vertex: result) {
			//~ vertex += center;
		//~ }
		
	//~ }
};

bool isBadPos(const glm::vec3 pos, Node node) {
	return true;
	if (node.getWalls()[0] && (pos[0] > node.getCenter()[0] + node.getSize() - 0.01)) {
		return false;
	}
	if (node.getWalls()[1] && (pos[1] > node.getCenter()[1] + node.getSize() - 0.01)) {
		return false;
	}
	if (node.getWalls()[2] && (pos[2] > node.getCenter()[2] + node.getSize() - 0.01)) {
		return false;
	}
	if (node.getWalls()[3] && (pos[0] > node.getCenter()[0] - node.getSize() + 0.01)) {
		return false;
	}
	if (node.getWalls()[4] && (pos[1] > node.getCenter()[1] - node.getSize() + 0.01)) {
		return false;
	}
	if (node.getWalls()[5] && (pos[2] > node.getCenter()[2] - node.getSize() + 0.01)) {
		return false;
	}
	return true;
}
class MyFreeCameraMover : public FreeCameraMover {
	std::vector<std::vector<Node>> nodes;

	void update(GLFWwindow* window, double dt)
	{
		float speed = 1.0f;

		//Получаем текущее направление "вперед" в мировой системе координат
		glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

		//Получаем текущее направление "вправо" в мировой системе координат
		glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
			
		//Двигаем камеру вперед/назад
		glm::vec3 old_pos = _pos;
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		{
			_pos += forwDir * speed * static_cast<float>(dt);
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		{
			_pos -= forwDir * speed * static_cast<float>(dt);
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		{
			_pos -= rightDir * speed * static_cast<float>(dt);
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		{
			_pos += rightDir * speed * static_cast<float>(dt);
		}
		if ((0 < _pos[0] < nodes.size()) && (0 < _pos[1] < nodes.size())) {
			if (isBadPos(_pos,  nodes[(int)_pos[0]][(int)_pos[1]])) {
				_pos = old_pos;
			}
			return;
		}
		//-----------------------------------------

		//Соединяем перемещение и поворот вместе
		_camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);
		
		//-----------------------------------------

		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		//Обновляем матрицу проекции на случай, если размеры окна изменились
		_camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
	}
		public:
	MyFreeCameraMover(const std::vector<std::vector<Node>>& nodes): nodes(nodes){
	}
};



int hash(int i) {
	return i*2654435761;
}


class SampleApplication : public Application
{
public:
    std::vector<std::vector<MeshPtr>> _maze{10, std::vector<MeshPtr>(10)};

	std::vector<std::vector<Node>> nodes{10};
    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();
		//~ glEnable(GL_CULL_FACE);
		int P = 1000000000 + 7;
        //Создаем меш с кубом
        
        //~ for (int i = 0; i < 10; ++i) {
			//~ for (int j = 0; j < 10; ++j) {
				//~ Node node(glm::vec3(i, j, 0));
				//~ if (i == 0) {
					//~ node.removeWall(3);
				//~ }
			//~ }
		//~ }

		//~ std::random_device rd;     // only used once to initialise (seed) engine
		    // random-number engine used (Mersenne-Twister in this case)
		//~ std::uniform_int_distribution<int> uni(0, 20); // guaranteed unbiased
        //~ _cameraMover = std::make_shared<MyFreeCameraMover>(nodes);
        _cameraMover = std::make_shared<FreeCameraMover>();

        for (int i = 0; i < 10; ++i) {
			for (int j = 0; j < 10; ++j) {
				Node node(glm::vec3(i, j, 0));
				node.removeWall(2);
				if (i < 9) {
					srand(i + j * 10);
					if (rand() % 100 < 60) {
						//~ std::cout << i << j << 0 << std::endl;
						node.removeWall(0);
					}
				}
				if (j < 9) {
					srand(i + j * 10 + 100);
					if (rand() % 100 < 60) {
						node.removeWall(4);
					}
				}
				if (i > 0) {
					srand(i - 1 + j * 10);
					if (rand() % 100 < 60) {
						//~ std::cout << i << j << 1 << std::endl;
						node.removeWall(3);
					}
				}
				if (j > 0) {
					srand(i + (j - 1) * 10 + 100);
					if (rand() % 100 < 60) {
						node.removeWall(1);
					}
				}
				node.init();
				_maze[i][j] = node.getMesh();
				//~ _cube = makeCube(5);
				_maze[i][j]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
				nodes[j].push_back(node);
			}
		}
        //Создаем меш из файла
        //~ _bunny = loadFromFile("models/bunny.obj");
        //~ _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("499ShchelchkovData/shaderNormal.vert", "499ShchelchkovData/shader.frag");
    }

    void update() override
    {
        Application::update();

        //Вращаем кролика
        //~ float angle = static_cast<float>(glfwGetTime());

        //~ glm::mat4 mat;
        //~ mat = glm::translate(mat, glm::vec3(0.0f, 0.5f, 0.0));
        //~ mat = glm::rotate(mat, angle, glm::vec3(0.0f, 0.0f, 1.0f));

        //~ _bunny->setModelMatrix(mat);
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        for (auto row: _maze) {
			for (auto _cube: row) {
				_shader->setMat4Uniform("modelMatrix", _cube->modelMatrix());
				_cube->draw();
			}
		}

        //Рисуем второй меш
        //~ _shader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
        //~ _bunny->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
